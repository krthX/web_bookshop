﻿var ViewModel = function () {
    //var of document itself
    var self = this;
    //book's entities
    self.books = ko.observableArray();
    self.loadingBooks = ko.observable();
    self.detailBooks = ko.observable();
    self.detailBookExist = ko.observable();
    self.tempBook = ko.observable();
    //author's entities
    self.authors = ko.observableArray();
    self.loadingAuthors = ko.observable();
    self.detailAuthors = ko.observable();
    self.detailAuthorExist = ko.observable();
    self.tempAuthor = ko.observable();
    //category's entities
    self.categories = ko.observableArray();
    self.loadingCategories = ko.observable();
    self.detailCategories = ko.observable();
    self.detailCategoryExist = ko.observable();
    self.tempCategory = ko.observable();
    //others vars.
    self.error = ko.observable();
    self.showCategoryDetail = ko.observable();

    self.hasStartedUpdateCategory = ko.observable();

    //Route controller method that returns the list of records.
    var booksUri = '/api/books/';
    var authorUri = '/api/writers/';
    var categoryUri = '/api/categories/';

    //Function that uses ajax for get data in a JSON file.
    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    //Gets all books and saves them in  an observable array object.
    function getAllBooks() {
        ajaxHelper(booksUri, 'GET').done(function (data) {
            self.books(data);
            self.loadingBooks(false);
        });
    }

    //Gets all authors and saves them in  an observable array object.
    function getAllAuthors() {
        ajaxHelper(authorUri, 'GET').done(function (data) {
            self.authors(data);
            self.loadingAuthors(false);
        });
    }

    //Gets all categories and saves them in  an observable array object.
    function getAllCategories() {
        ajaxHelper(categoryUri, 'GET').done(function (data) {
            self.categories(data);
            self.loadingCategories(false);
        });
    }

    //Gets a item in the table of books in the db and saves him in an observable object.
    self.getBooksDetail = function (item) {
        ajaxHelper(booksUri + item.ID, 'GET').done(function (data) {
            self.detailBookExist(true);
            self.tempBook(item);
            self.detailBooks(data);
        });
    };

    //
    self.getAuthorsDetail = function (item) {
        ajaxHelper(authorUri + item.ID, 'GET').done(function (data) {
            self.detailAuthorExist(true);
            self.tempAuthor(item);
            self.detailAuthors(data);
        });
    };

    //
    self.getCategoriesDetail = function (item) {
        //alert(item.ID);
       if (!self.detailCategoriesExist) {
            ajaxHelper(categoryUri + item.ID, 'GET').done(function (data) {
                self.detailCategoryExist(true);
                self.tempCategory(item);
                self.detailCategories(data);
            });
        };
    };

    //It contains several elements that keep data for a new book.
    self.newBook = {
        Author: ko.observable(),
        Title: ko.observable(),
        ReleaseDate: ko.observable(),
        Category: ko.observable()
    };

    //It contains several elements that keep data for a new book.
    self.newAuthor = {
        Name: ko.observable(),
        Surname: ko.observable(),
        BirthDate: ko.observable(),
        Description: ko.observable()
    };

    //It contains several elements that keep data for a new category.
    self.newCategory = {
        Name: ko.observable(),
        Description: ko.observable()
    };

    //Add a new book in the database and updates observable object.
    self.addBook = function (formElement) {
        var book = {
            Title: self.newBook.Title(),
            ReleaseDate: self.newBook.ReleaseDate(),
            ImgFront: self.newBook.Title().replace(" ") + ".jpg",
            AuthorID: self.newBook.Author().ID,
            CategoryID: self.newBook.Category().ID
        }
        ajaxHelper(booksUri, 'POST', book).done(function (item) {
            self.books.push(item);
        });
    }

    //Add a new writer in the database and updates observable object.
    self.addWriter = function (formElement) {
        var writer = {
            Name: self.newAuthor.Name(),
            Surname: self.newAuthor.Surname(),
            BirthDate: self.newAuthor.BirthDate(),
            Description: self.newAuthor.Description()
        }

        ajaxHelper(authorUri, 'POST', writer).done(function (item) {
            self.authors.push(item);
        });
    };

    //Add a new category in the database and updates observable object.
    self.addCategory = function (formElement) {
        var category = {
            Name: self.newCategory.Name(),
            Description: self.newCategory.Description()
        }

        ajaxHelper(categoryUri, 'POST', category).done(function (item) {
            self.categories.push(item);
        });
    };

    //Delete a book
    self.deleteBook = function (item) {
        ajaxHelper(booksUri + self.detailBooks().ID, 'DELETE').done(function (data) {
            self.books.remove(self.tempBook());
            self.detailBookExist(false);
        });
    };

    //Delete a writer
    self.deleteWriter = function () {
        ajaxHelper(authorUri + self.detailAuthors().ID, 'DELETE').done(function (data) {
            self.authors.remove(self.tempAuthor());
            self.detailAuthorExist(false);
        });
    };

    //Delete a category
    self.deleteCategory = function () {
        ajaxHelper(categoryUri + self.detailCategories().ID, 'DELETE').done(function (data) {
            self.categories.remove(self.tempCategory());
            self.detailCategoryExist(false);
        });
    };

    //
    self.varUpdateCat = {
        ID: ko.observable(),
        Name: ko.observable(),
        Description: ko.observable()
    };

    self.initCategoryUpdate = function () {
        self.showCategoryDetail(false);
        self.hasStartedUpdateCategory(true);
        self.varUpdateCat.ID(self.detailCategories().ID);
        self.varUpdateCat.Name(self.detailCategories().Name);
        self.varUpdateCat.Description(self.detailCategories().Description);
    };

    //
    self.UpdateCategory = function () {
        var catUp = {
            ID: self.varUpdateCat.ID(),
            Name: self.varUpdateCat.Name(),
            Description: self.varUpdateCat.Description()
        }
        ajaxHelper(categoryUri + self.varUpdateCat.ID(), 'PUT', catUp).done(function (data) {
            alert('Category with id: ' + data.ID + ' was updated');
            self.detailCategories(self.varUpdateCat);
            self.hasStartedUpdateCategory(false);
            self.showCategoryDetail(true);
            //self.categories.removeAll();
            //getAllCategories();
            self.categories.replace(self.tempCategory(), catUp);
            self.categories.valueHasMutated();
            //self.categories.replace(self.tempCategory(), self.varUpdateCat);
        });
    };

    // Fetch the initial data.
    getAllBooks();
    getAllAuthors();
    getAllCategories();
    self.loadingBooks(true);
    self.loadingAuthors(true);
    self.loadingCategories(true);
    self.detailBookExist(false);
    self.detailAuthorExist(false);
    self.detailCategoryExist(false);
    self.hasStartedUpdateCategory(false);
    self.showCategoryDetail(true);
};

ko.applyBindings(new ViewModel());