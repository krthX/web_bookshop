namespace WEBAPI_BOOKSHOP.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using WEBAPI_BOOKSHOP.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<WEBAPI_BOOKSHOP.Models.WEBAPI_BOOKSHOPContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WEBAPI_BOOKSHOP.Models.WEBAPI_BOOKSHOPContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //All Data

            //Categories
            context.Categories.AddOrUpdate(c => c.ID,
                new Category { ID = 1, Name = "Drama", Description = "Drama" },
                new Category { ID = 2, Name = "Action", Description = "Action" },
                new Category { ID = 3, Name = "Thriller", Description = "Thriller" },
                new Category { ID = 4, Name = "Adventure", Description = "Adventure" },
                new Category { ID = 5, Name = "Tragedie", Description = "Tragedie" },
                new Category { ID = 6, Name = "Fancy", Description = "Fancy" }
            );

            //Writers
            context.Writers.AddOrUpdate(w => w.ID,
                new Writer { ID = 1, Name = "William", Surname = "Shakespeare", BirthDate = "1564", Description = "Bard of Avon" },
                new Writer { ID = 2, Name = "Ernest", Surname = "Hemingway", BirthDate = "1899", Description = "Writer and journalist" },
                new Writer { ID = 3, Name = "Jorge Luis", Surname = "Borges", BirthDate = "1899", Description = "One of the most renowned scholars" },
                new Writer { ID = 4, Name = "Charles", Surname = "Dickens", BirthDate = "1812", Description = "The foremost of the Victorian era." }
            );

            //Books
            context.Books.AddOrUpdate(b => b.ID,
               new Book { ID = 1, Title = "Romeo and Juliet", ReleaseDate = "1579", ImgFront = "Romeo'nJuliet.jpg", AuthorID = 1, CategoryID = 5 },
               new Book { ID = 2, Title = "A Christmas Carol", ReleaseDate = "1843", ImgFront = "Christmas.jpg", AuthorID = 4, CategoryID = 6 },
               new Book { ID = 3, Title = "The Death and The Compass", ReleaseDate = "1942", ImgFront = "death.jpg", AuthorID = 3, CategoryID = 6 },
               new Book { ID = 4, Title = "Men Without Women", ReleaseDate = "1927", ImgFront = "men.jpg", AuthorID = 2, CategoryID = 1 },
               new Book { ID = 5, Title = "Hamlet", ReleaseDate = "1599-1601", ImgFront = "Hamlet.jpg", AuthorID = 1, CategoryID = 5 },
               new Book { ID = 6, Title = "Oliver Twist", ReleaseDate = "1837-1839", ImgFront = "Oliver.jpg", AuthorID = 4, CategoryID = 4 }
            );
        }
    }
}
