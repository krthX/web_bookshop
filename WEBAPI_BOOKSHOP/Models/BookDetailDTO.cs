﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAPI_BOOKSHOP.Models
{
    public class BookDetailDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public string AuthorName { get; set; }
        public string CategoryName { get; set; }
    }
}