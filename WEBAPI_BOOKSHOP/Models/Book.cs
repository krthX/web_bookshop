﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WEBAPI_BOOKSHOP.Models
{
    public class Book
    {
        [Required]
        public int ID { get; set; }
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public string ImgFront { get; set; }

        //Author Foreign key
        [Required]
        public int AuthorID { get; set; }
        
        //Author Navigation Property
        public Writer Author { get; set; }

        //Category Foreign Key
        [Required]
        public int CategoryID { get; set; }
        
        //CAtegory Navigation Property
        public Category Category { get; set; }
    }
}