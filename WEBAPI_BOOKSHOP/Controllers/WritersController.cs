﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WEBAPI_BOOKSHOP.Models;

namespace WEBAPI_BOOKSHOP.Controllers
{
    public class WritersController : ApiController
    {
        private WEBAPI_BOOKSHOPContext db = new WEBAPI_BOOKSHOPContext();

        // GET api/Writers
        public IQueryable<Writer> GetWriters()
        {
            return db.Writers;
        }

        // GET api/Writers/5
        [ResponseType(typeof(Writer))]
        public async Task<IHttpActionResult> GetWriter(int id)
        {
            Writer writer = await db.Writers.FindAsync(id);
            if (writer == null)
            {
                return NotFound();
            }

            return Ok(writer);
        }

        // PUT api/Writers/5
        public async Task<IHttpActionResult> PutWriter(int id, Writer writer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != writer.ID)
            {
                return BadRequest();
            }

            db.Entry(writer).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WriterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Writers
        [ResponseType(typeof(Writer))]
        public async Task<IHttpActionResult> PostWriter(Writer writer)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            db.Writers.Add(writer);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = writer.ID }, writer);
        }

        // DELETE api/Writers/5
        [ResponseType(typeof(Writer))]
        public async Task<IHttpActionResult> DeleteWriter(int id)
        {
            Writer writer = await db.Writers.FindAsync(id);
            if (writer == null)
            {
                return NotFound();
            }

            db.Writers.Remove(writer);
            await db.SaveChangesAsync();

            return Ok(writer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WriterExists(int id)
        {
            return db.Writers.Count(e => e.ID == id) > 0;
        }
    }
}