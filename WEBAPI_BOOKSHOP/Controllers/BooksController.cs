﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using WEBAPI_BOOKSHOP.Models;

namespace WEBAPI_BOOKSHOP.Controllers
{
    public class BooksController : ApiController
    {
        private WEBAPI_BOOKSHOPContext db = new WEBAPI_BOOKSHOPContext();

        // GET api/Books
        public IQueryable<BookDTO> GetBooks()
        {
            var books = from b in db.Books
                        select new BookDTO()
                        {
                            ID = b.ID,
                            Title = b.Title,
                            AuthorName = b.Author.Name + " " + b.Author.Surname
                        };

            return books;
        }

        // GET api/Books/5
        [ResponseType(typeof(Book))]
        public async Task<IHttpActionResult> GetBook(int id)
        {
            var book = await db.Books.Include(b => b.Author).Select(b => new BookDetailDTO()
                 {
                     ID = b.ID,
                     Title = b.Title,
                     ReleaseDate = b.ReleaseDate,
                     AuthorName = b.Author.Name + " " + b.Author.Surname,
                     CategoryName = b.Category.Name
                 }).SingleOrDefaultAsync(b => b.ID == id);
            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        // PUT api/Books/5
        public async Task<IHttpActionResult> PutBook(int id, Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != book.ID)
            {
                return BadRequest();
            }

            db.Entry(book).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Books
        [ResponseType(typeof(Book))]
        public async Task<IHttpActionResult> PostBook(Book book)
        {
           /** if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }*/

            db.Books.Add(book);
            await db.SaveChangesAsync();

            // New code:
            // Load author name
            db.Entry(book).Reference(x => x.Author).Load();

            var dto = new BookDTO()
            {
                ID = book.ID,
                Title = book.Title,
                AuthorName = book.Author.Name + " " + book.Author.Surname
            };

            return CreatedAtRoute("DefaultApi", new { id = book.ID }, dto);

            //return CreatedAtRoute("DefaultApi", new { id = book.ID }, book);
        }

        // DELETE api/Books/5
        [ResponseType(typeof(Book))]
        public async Task<IHttpActionResult> DeleteBook(int id)
        {
            Book book = await db.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }

            db.Books.Remove(book);
            await db.SaveChangesAsync();

            return Ok(book);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BookExists(int id)
        {
            return db.Books.Count(e => e.ID == id) > 0;
        }
    }
}